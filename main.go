package main

import (
	"fmt"
)

func main() {
	var weight, height float64

	fmt.Print("请输入体重(kg):")
	fmt.Scanln(&weight)

	fmt.Print("请输入身高(m):")
	fmt.Scanln(&height)

	bmi := weight / (height * height)
	fmt.Printf("您的 BMI 指数为 %.2f\n", bmi)

	category := getBMICategory(bmi)
	fmt.Printf("BMI 分类：%s\n", category)
}

func getBMICategory(bmi float64) string {
	switch {
	case bmi < 18.5:
		return "偏瘦"
	case bmi >= 18.5 && bmi < 24:
		return "正常"
	case bmi >= 24 && bmi < 28:
		return "超重"
	default:
		return "肥胖"
	}
}
